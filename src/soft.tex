\section{Software interface}

\label{annex:soft}Every part of the robot is connected to a computer, which takes care of the communication with the device and hosts the different parts of the AI. The drivers and the global coordination is handled by ROS nodes.
\subsection{ROS}
ROS stands for Robot Operating System, an open-source free framework intended to provide a toolbox for robot applications in an operating system-like fashion through abstraction of the hardware, message-passing, higher level tools, etc.

The most important concept in ROS is the Computation Graph layer. It is a peer-to-peer network of ROS processes mainly composed of nodes, messages, services and topics.

\begin{itemize}
\item[--] \tbf{Nodes} are processes that perform an individual task in the whole scheme (\tit{e.g.} control of an actuator, processing of data from a sensor, path-planning, \dots).
\item[--] \tbf{Messages} are the data structures used by nodes to communicate with one another.
\item[--] \tbf{Services} are pairs of messages used for a request and a reply, when one node has to get information from another node specifically.
\item[--] \tbf{Topics} are channels used by nodes to send data in a many-to-many scheme. Nodes can publish on topics and subscribe on (other) topics. A topic can be seen as a message bus which anyone can connect to and send/recieve messages.
\end{itemize}

\emph{ROS packages} are generally a set of nodes designed to accomplish a given task or to handle specific hardware. The most popular packages can be installed and kept up-to-date through a repository. ROS nodes are written in C++ or in Python.

\subsection{MoveIt! for the UR5}
MoveIt! is a software designed for mobile manipulation. It comes as a ROS package and provides a platform allowing motion planning with collision detection, manipulation, 3D perception, kinematics, control and navigation.

The ROS-Industrial project has worked towards the compatibility between MoveIt! and the UR5. At the University of Liège, the configuration has been refined in order to support the gripper and avoid planning trajectories that would damage the hardware by clashing the gripper on the arm. MoveIt! also comes with a GUI interface (it uses another ROS package called \ttt{rviz}), allowing for a very nice visualization of the state of the robot.

To achieve motion planning, MoveIt! has to find the inverse kinematics of the goal and plan a compatible motion from the current states of the joints to those of the goal. The inverse kinematics is computed by a tool called IKFast, a technology developed by OpenRAVE, and the motion plan uses OMPL, the Open Motion Planning Library.

\subsubsection{Quick word about the OMPL}\label{subannex:ompl}

The OMPL uses sampling-based algorithms, which means that the resulting trajectories will not be deterministically computed. Attempting the same motion several times will thus not necessarily generate the same trajectory. Some of its most interesting (and most recent) planners are STRIDE \cite{SPARSE} and LBTRRT \cite{LBTRRT}, however, they have not been integrated in the version of Moveit! used in our framework. A more thorough discussion about OMPL and the choice of the planner will be held in Appendix \ref{annex:plan}.

The OMPL will not check for collisions: this is something MoveIt! has to take care of. Oftentimes, the motion plan computed is incompatible with the constraints specific to the robot (\tit{e.g.} self-collisions, collisions with the environment\dots), and MoveIt! has to request a new trajectory from OMPL. After a given number of unsuccessful attempts, MoveIt! will give up and let the user decide what to do.

\subsection{Robotiq}
Robotiq provides a ROS package to send basic commands to the gripper as well as a driver which handles the communication.

\subsection{Sensors}
With the sensors came a GUI showing the data the sensors as well as a very rudimentary C++ interface allowing to sample data from one of the 10 sensors at a time. 

This interface was not practical, as it made data acquisition from more than one sensor extremely slow and inefficient. It thus has been carefully modified in order to allow the operator to acquire data from any sensor as fast as possible. Unfortunately, communication is slow and we cannot acquire data from the 7 functional sensors faster than 6 times per second.

\subsection{API}

ROS comes with a lot of nice features, but uses a complicated an convoluted build system. In order to allow easy manipulation of the hardware, a C++ interface that provides very high-level functionalities with simpler compilation dependencies had been designed over the last summer.

This interface, however, was not quite finished when work actually began on the robot. A significant part has been rewritten to make it more convenient to use.
%reofrmulate this

For example, the main high-level functionality of the interface for the arm only allowed to move the end-effector of the UR5 to a given pose, and it was not enough to solve our problem, as discussed in Section \ref{sec:approach}. We can now tell the robot to follow a cartesian trajectory along given waypoints with some constraints and thus prevent major joints reconfigurations during critical parts of the movement. 

The interface uses quaternions to describe poses (see Appendix \ref{annex:quat} for a description of quaternions) and the \ttt{move\_group} C++ interface for sending the commands to MoveIt!. An overview of the interface of MoveIt! interface is shown in Figure \ref{fig:moveit-overview}.

An interface had been written for the gripper as well, which provides an easy and intuitive way to retrieve the state of the gripper, craft a command and execute it. It uses ROS messages and connects to the node driving of the gripper.

\imghere{moveit-overview}{.9}{Overview of the \ttt{move\_group} interface (courtesy of the Moveit! wiki)}

\subsection{Implementation details for FQI}
The fitted Q iteration has been implemented in Python using \ttt{scikit-learn}. It parses the pre-filtered log file containing $\mcal{F}$ and follows the procedure stated in Algorithm \ref{alg:fqi-algo}. The resulting model $\mcal{Q}^N$ is then stored as a file thanks to the \ttt{pickle} library for further use.

\subsection{Implementation details for the decision procedure}
% \textcolor{BrickRed}{\tbf{implementation of a ros node communication and a decision node}}
The decision procedure is written in Python as a ROS service that loads the model generated previously with the fitted Q iteration. The node requesting a decision sends the raw data from the sensors and the state of the fingers. This data is filtered as required by our model in a state $\mbf{s}$ and the decision function $f(\mbf{s})$ described in Section \ref{subsubs:useQ} is called. The resulting decision is then returned and processed by the robot.