\section{Grasping}\label{sec:grasping}

In this part, we will discuss our approach to the grasping procedure assessment and how we manage to decide as early as possible to stop when the grasping fails. The arm does not move anymore, and we look at what happens while the gripper is closing. This section contains the core part of this master's thesis, as we address the problem of deciding, based upon the data available to us from the sensors, whether the grasp is failing or not. We want to model grasp outcomes from real time tactile data and, to achieve this, we plan to learn the model parameters automatically via reinforcement learning. The algorithm that we will choose has to be able to take its decisions from an input that can be processed as soon as the data comes.

We will first define what we consider a success and a failure. Then, we will talk about the kind of data available to us and go over the process of experimental acquisition and labeling of this data. We will then show how we use a learning algorithm called the fitted Q Iteration \cite{FQI} to build a model that will allow us to take decisions in an online fashion about the expected final outcome of the grasp.

%This part is when the gripper will be closed and the data from the sensors will be recorded and fed to a model which will decide, should the grasping procedure fail, if the grasp has failed and has to abort.

\subsection{Definition of a successful grasp}\label{subs:successfulgrasp}

From a general taxonomy \cite{Taxonomy} recently issued that has been widely used among the grasping community, a grasp is defined as follows: “A grasp is every static hand posture with which an object can be held securely with one hand.” However, we will need to consider some grasps as not suitable and thus draw the line between a grasp that fails and one that does not.

In Section \ref{subs:underact}, we discussed about the benefits of underactuation and how well the gripper behaves when grasping objects in a non optimal way (hence making failure cases rare and unlikely). This led us to put an emphasis on the “securely” part of the definition: the operator will use their judgment to label the experiment as a failure if the grasp does not seem secure enough to him.

We can see in Figures \ref{fig:grasps_1} and \ref{fig:grasps_2} different typical cases: on the left of Figure \ref{fig:grasps_1} we have a power grasp and a precision grasp on its right. The power grasp is a grasp where the palm is involved to secure the hold.  A precision grasp, on the contrary, involves a pinch and mainly relies on the distal phalanges. In Figure \ref{fig:grasps_2}, we have what turns out to be an unstable grasp on the left, which is thus deemed incorrect, and an incomplete grasp on the right which is unsatisfactory as well. %If we were to try to achieve a power grasp, we would discard the precision grasp despite the fact that the grasp is stable and steady, for it is considered as an unintended behavior with respect to the part-based planner\footnote{note for the editor: TODO standardize spelling wrt american and british english}.

\begin{figure}[H]
\begin{adjustwidth}{-1cm}{-1cm}
  \begin{center}
    \begin{subfigure}{0.5\textwidth}
      \centering
      \includegraphics[trim = 10px 80px 200px 50px, clip, width=\textwidth]{img/savon1_2}
    \end{subfigure}%
    ~
    \begin{subfigure}{0.5\textwidth}
      \centering
      \includegraphics[trim = 10px 80px 200px 50px, clip, width=\textwidth]{img/savon2_2}
    \end{subfigure}
  \caption{Power grasp and precision grasp}\label{fig:grasps_1}
  \end{center}
 \end{adjustwidth}
\end{figure}

\begin{figure}[H]
\begin{adjustwidth}{-1cm}{-1cm}
  \begin{center}
    \begin{subfigure}{0.5\textwidth}
      \centering
      \includegraphics[trim = 10px 80px 200px 50px, clip, width=\textwidth]{img/savon4_2}
    \end{subfigure}
    ~
    \begin{subfigure}{0.5\textwidth}
      \centering
      \includegraphics[trim = 10px 80px 200px 50px, clip, width=\textwidth]{img/savon4_2__}
    \end{subfigure}
  \caption{Unstable grasp and incomplete grasp}\label{fig:grasps_2}
  \end{center}
 \end{adjustwidth}
\end{figure}

\subsection{Monitoring the sensors}

While we will be monitoring the sensors, we will have to face a serious bottleneck: in order to retrieve the data from each of the seven sensors, we need around 160 ms due to hardware constraints. Taking this into account, and as the fingers can move from position 0 to 255, we will retrieve the data from the sensors every 10 positions with the gripper closing as slowly as possible. We will now define more formally the data coming from the sensors.

%We will start by retrieving a binary information for each sensor telling whether the sensor is in contact with something or not and try to use that as a sufficiently significant feature for the learning. // → no: we will send everything we can and the python code will do the post-processing if any is required

\subsection{Formalization of the data that can be acquired}\label{subs:datadef}
Let $\mcal{S}$ be the set of all possible states of the gripper, a measured state $\tbf{m} \in \mcal{S}$ is defined by:

$$\tbf{m} = (\mbf{c}, \mbf{f})$$

where $\mbf{c} = (\tbf{l}_{1}, \tbf{l}_{2}, \tbf{l}_{3}, \tbf{l}_{4}, \tbf{l}_{5}, \tbf{l}_{6}, \tbf{l}_{7}, \tbf{l}_{8}, \tbf{l}_{9}, \tbf{l}_{10})$ is the tactile data from the ten sensors with $\tbf{l}_i \in \mcal{C}_i$ the set of the $n$-tuples containing the integer values that can be measured by the $n$ cells from the $i$\textsuperscript{th} sensor for $i \in \{1, 2, \dots, 10\}$ and $\mbf{f} = (f_{1}, f_{2}, f_{3})$ corresponds to the triplet of the positions of the fingers, with $f_{1}, f_{2}$ and $f_{3} \in \mbb{N}$ and $\leq 255$. 

An experiment $\mcal{E}$ is a sequence of 25 states, each of which describes the state of the gripper at a given time. The fingers have a position ranging from 0 (fully opened) to 255 (fully closed). The $i$\textsuperscript{th} measurement $\tbf{m}_i$ is made as soon as one of the fingers reaches the position $10i$. Then, once the fingers are all stationary, the remaining measurements are made until we have a total of 25 samples. A successful experiment is denoted by $\mcal{E}_s$ and corresponds to a satisfying grasp, an unsuccessful experiment is denoted by $\mcal{E}_f$ and corresponds to a grasp that failed.

% An experiment $\mcal{E}$ is a sequence of 25 states, each of which describes the state of the gripper at a given position. The fingers have a position ranging from 0 (fully opened) to 255 (fully closed). The sensors are measured as soon as one of the three fingers reaches a given value that gets increased afterwards, then, once the fingers are all stationary, until we have a total of 25 samples. A successful experiment is denoted by $\mcal{E}_s$ and corresponds to a satisfying grasp, an unsuccessful experiment is denoted by $\mcal{E}_f$ and corresponds to a grasp that failed.

\subsection{Learning grasps}

Experimental data is required to build any model through a machine learning algorithm. In this subsection, the focus will be on the data collection. As stated before in Section \ref{subs:underact}, simulation is not possible with the hardware available: the data that will be used for learning has to be acquired through manual experiments.

Depending on what algorithm we will use for our decision procedure, the data may need some filtering before being processed. However, it is important to log the data as complete experiments $\mcal{E}$: the process might evolve and it could then require extra information that would have been lost otherwise through filtering, which means going one more time through the tedious process of experimental data acquisition.

%Unfortunately, the proximal sensors (\tit{i.e.} sensors 3, 6 and 9) stopped working in our setup, and we couldn't log any data from them. However, this does not change our approach: it is simply as if we only had two sensors on each finger and we will discard the defective hardware from now on.

We used only the distal and middle pads to record the tactile data, but this does not change our approach: it is simply as if we only had two sensors on each finger.

In Figure \ref{fig:typical-experiment}, we show an example of the initial and final states of an experiment. Sensors are monitored and logged in-between, every experiment providing 25 states. The experiment is then labeled according to the success evaluation made by the operator from the definition of a successful grasp stated in Section \ref{subs:successfulgrasp}.

\begin{figure}[H]
\begin{adjustwidth}{-1cm}{-1cm}
  \begin{center}
    \begin{subfigure}{0.3\textwidth}
      \centering
      \includegraphics[trim = 100px 160px 320px 150px, clip, width=\textwidth]{img/savon1_1}
    \end{subfigure}%
    ~$\Longrightarrow$
    \begin{subfigure}{0.3\textwidth}
      \centering
      \includegraphics[trim = 100px 160px 320px 150px, clip, width=\textwidth]{img/savon1_2}
    \end{subfigure}
  \caption{Initial state and final state of a typical experiment}\label{fig:typical-experiment}
  \end{center}
 \end{adjustwidth}
\end{figure}

%As an example, we have made a total of 22 full experiments for a given object by moving the position of the object between grasp attempts. 14 attempts were labeled as successful. \textcolor{BrickRed}{\tbf{These numbers will change with the overshots.}}

Figure \ref{fig:soap-experiments} displays an interesting sample of what happened during the experiments. The green and red borders are used to distinguish the successful attempts from the unsuccessful attempts.

On the first experiment shown in that figure, we successfully grasp a spray bottle. The second experiment shows a successful precision grasp of a mug whose handle is inside the space of the gripper but is not in contact with anything. The third experiment shows how the same mug with a rotation of 90° can lead to a grasp that is not secure as the fingers close on the handle. The two experiments that follow are perfect precision grasps, and the last experiment shows an attempt of a precision grasp on a tilted shampoo bottle that leads to an unstable grasp.

Figure \ref{fig:experiment-dat-graphs} then shows a set of graphs displaying the complete data acquired through an attempt at grasping a wide but short PVC tube. This experiment resulted in the grasp shown in Figure \ref{fig:experiment-dat-photo} and was labeled as a failed grasp, as one of the fingers did not touch the tube.

\input{src/sample-experiments.fig}

\pgfplotstableread{src/grasp-data-ex3.dat}{\GRSP}
\begin{figure}[H]
  \centering
    \input{src/graph-exp-dat.tex}
    \caption{Tactile and finger values acquired from the experiment whose outcome is shown in Figure \ref{fig:experiment-dat-photo}}
    \label{fig:experiment-dat-graphs}
\end{figure}

\begin{figure}[H]
% \begin{adjustwidth}{-1cm}{-1cm}
  \begin{center}
      \centering
      %\includegraphics[trim = 80px 140px 300px 130px, clip, width=0.6\textwidth]{img/expsoap/savon10_2}
      \includegraphics[trim = 100px 140px 280px 130px, clip, width=0.6\textwidth]{img/pvc2}
      \caption{Final state of the experiment shown in Figure \ref{fig:experiment-dat-graphs}, we can see that finger 1 missed the objective}\label{fig:experiment-dat-photo}
    \end{center}
  % \end{adjustwidth}
\end{figure}

\input{src/fqi.tex}

% \subsection{Sliding window}
% Talk about the sliding window approach (mathematical formalization and implementation)