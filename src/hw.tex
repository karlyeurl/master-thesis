\section{Hardware description}

Generally speaking, the hardware required for grasp synthesis consists of an actuated arm and a gripper. At the University of Liège, we use a Universal Robots UR5 arm and a Robotiq S Model gripper equipped  with tactile sensors. The data from which we base the grasping plan on is obtained through the use of a Microsoft Kinect 360 fixed on the same structure as the arm. The software interface used to drive all this hardware is described in Appendix \ref{annex:soft}.

In this section, we mention \tbf{forward} and \tbf{inverse} kinematics. Forward kinematics (FK) allow to express the pose of a joint given the state of each actuator. Inverse kinematics (IK) allow to express a possible configuration of the actuators that will enable a joint to reach a given pose.

\subsection{Actuated arm}

The UR5 is a common robotic arm used for industrial purposes. It has six degrees of freedom and can be programmed and controlled either through a built-in tactile interface or remotely through a network interface.

\imghere{ur5}{.56}{The UR5 arm (courtesy of Universal Robots)}
%go into the ur5 documentation to find cool characteristics as the access space/sphere and such // TODO image courtesy of

The UR5 has an analytical expression for both the forward and inverse kinematics, which is an asset in motion planning.

\subsection{Gripper}

The Robotiq S Model gripper is an underactuated gripper with three fingers designed for industrial purposes as well. It is driven by a network interface, and every finger can be controlled individually in position, force and speed. The fingers are labeled for further reference.

% \imghere{robotiqSmodel}{.6}{Robotiq S Model gripper}
\begin{figure}[H]
\begin{adjustwidth}{-1cm}{-1cm}
\begin{center}
\begin{tikzpicture}
    \node[anchor=south west,inner sep=0] (image) at (0,0) {\includegraphics[width=0.6\linewidth]{img/robotiqSmodel}};
    \begin{scope}[x={(image.south east)},y={(image.north west)}]
          \draw[blue,ultra thick,rounded corners] (0.578,0.53) -- (0.65,0.8) -- (0.65,0.95) -- (0.73,0.95) node[anchor=west]{Finger 2} -- (0.76, 0.83) -- (0.73, 0.62) -- (0.7, 0.5) -- cycle;
          \draw[red,ultra thick,rounded corners] (0.4, 0.41) -- (0.35, 0.36) -- (0.31, 0.36) -- (0.24, 0.6) -- (0.22, 0.78) -- (0.265, 0.95) node[anchor=east]{Finger 1} -- (0.33, 0.95) -- (0.34162, 0.77);
          \draw[orange,ultra thick,rounded corners] (0.56,0.4) -- (0.4, 0.4) -- (0.35, 0.55) -- (0.34, 0.77) -- (0.35, 0.82) -- (0.38, 0.93) -- (0.455, 0.93) node[anchor=west]{Finger 3} -- (0.46, 0.75) -- cycle;
 %          \draw[help lines,xstep=.1,ystep=.1] (0,0) grid (1,1);
	% \foreach \x in {0,1,...,9} { \node [anchor=north] at (\x/10,0) {0.\x}; }
	% \foreach \y in {0,1,...,9} { \node [anchor=east] at (0,\y/10) {0.\y}; }
    \end{scope}
\end{tikzpicture}
\caption{The Robotiq S Model gripper (courtesy of Robotiq)}\label{fig:robotiqSmodel}
\end{center}
\end{adjustwidth}
\end{figure}

\subsubsection{Underactuation: a blessing and a curse}\label{subs:underact}
Every finger is closed by only one actuator, but has three articulated phalanges (hence, it is called \emph{underactuated}). Underactuation allows, to a certain extent, the other sections of the finger to continue wrapping the object when a phalanx enters in contact with an obstacle. It has multiple advantages, as it makes the grasping more adaptive, thus much more likely to succeed, and requires less actuators, which lowers the cost of the hardware.

The price to pay is that it makes forward kinematics a complex problem due to nonlinear and nonsmooth couplings between the degrees of freedom. In this work, we will not need to solve this problem, but it has partly been addressed by G. Franchi and K. Hauser in their technical report \cite{RobotiQFK}. However, a full mathematical model of the gripper's dynamics would have made simulation possible and provided an convenient way to easily generate large quantities of data for learning.

\subsection{Sensing devices}
\subsubsection{Vision}
We acquire images from a Microsoft Kinect 360. The Kinect offers a reasonably cheap and easy-to-use way to acquire depth images, and its popularity among robotics enthusiasts has brought many tools to process incoming data from this sensor.
\imghere{kinect}{.6}{A Kinect device (courtesy of Wikimedia Commons)}

\subsubsection{Load cells}
There are two types of sensors mounted on the robot: an ATI torque sensor fit between the gripper and the arm (which is not used in our application) and specifically designed extremely sensitive load cells fixed on the palm and the fingers. 

There are 10 tactile sensors in total: one pad for the palm, and three per finger. Each pad has a specific number of sensing units: 12 on the palm, referred to as the proximal sensor, 9 on each of the 6 medials (the phalanges close to the palm) and 8 on each of the 3 distals (the three remaining phalanges). These sensing units can be processed to retrieve static data about the pressure applied on the sensors.
%\twoimghere{rsui_global}{.7}{\\ ~\\ ~\\}{rsui_stat}{.7}{Sensors interface (left shows all the sensors, right shows details for a sensor)}
\imghere{rsui_global}{.7}{Tactile Sensor Visualiser for all the sensors at once}
\imghere{rsui_stat}{.7}{Tactile Sensor Visualiser interface for one given sensor}

There is a dynamic mode as well which allows to show the variation of pressure experienced by the cells, but we will not use it in this work.
%put some more documentation here, perhaps about the dyndata?

\subsection{Mounting}
The mounting, designed for this particular setup, has been thought with stability and robustness in mind. It is able to support any configuration of the robot and embeds the sensing devices, which, for example, prevents the need of a recalibration of the Kinect when the whole structure moves slightly.
%put a representation of the mounting here
%put a picture of the complete setup here
\begin{figure}[H]
\begin{adjustwidth}{-1cm}{-1cm}
  \begin{center}
    \begin{subfigure}{.5\linewidth}
      \centering
      \includegraphics[width=\linewidth]{img/ur5_pied}
    \end{subfigure}%
    ~
    \begin{subfigure}{.5\linewidth}
      \centering
      \includegraphics[trim = 10px 80px 200px 50px, clip, width=\linewidth]{img/mounting}
    \end{subfigure}
  \caption{Design of the structure and current mounting}\label{fig:mounting}
  \end{center}
\end{adjustwidth}
\end{figure}