\section{Introduction}

Robotics is a broad field where a lot of work is yet to be done to efficiently accomplish tasks automatically. Stable and reliable grasping is one of those problems that have to be overcome. In the future, robots will most certainly have a much wider use than they have now, from the unsupervised accomplishment of domestic tasks to reactive and smart industrial automation.

\subsection{Grasping objects: a multifaceted problem}

Grasping an object, \tit{i.e.} using a gripper to successfully take an object and lift it, is a problem where the approach mostly depends on the data available about the situation and on the situation itself. The solution will not be the same if there is a full 3D model of the object or if there is only a partial pointcloud from one point of view. It also depends on the environment, the feedback available when grasping and the type of grasping that we try to achieve: \tit{form closure}, when the gripper is stiff and the object is maintained by its own shape, or \tit{force closure}, when stability is maintained by squeezing the object.

%We will focus on a subproblem where only a partial pointcloud is available .
%There is a lot of room for research in this area, and it is a common practice to study thouroughly a very specific aspect of the problem.

%Usually, reasearch is made with \tit{some} assumptions in order to extend the .

\subsection{Description of the subproblem of interest}
Here, we will be focusing on the grasping of objects of undefined shape which are seen from a single point of view given the feedback of tactile sensors. The problem of using incomplete pointclouds to find a grasping approach is addressed by Renaud Detry by a mechanism that we will call the part-based planner \cite{PBP2}. As the pointcloud data is acquired from the other side of the arm to avoid any obstruction, we will be likely to try grasping the objects from a side we have little knowledge of. Thus it is important to assess the quality of the grasping while the motion takes place, which will be our focus.

The goal of this work is to decide, based upon the tactile data acquired through the approach procedure and the tactile data obtained while closing the gripper, whether the grasping is going as intended or if something unexpected happens, in which case the process has to be aborted as soon as possible to minimize the impact of the unpredicted event on the environment.

\subsection{Sources of failure}

Various reasons can compromise the success of the process. There can be an error in the calibration between the camera and the robot that will lead to an offset between the actual location of the object and where the robot believes the object lays. Partial pointclouds imply blind spots where the object can have an unexpected shape (\tit{e.g.} a mug whose handle lays in that blind spot). The object could have been moved between the acquisition and the motion. The part-based planner might also send unsuitable grasping plans, the camera shows artifacts due to poor lighting… Basically, everything that happens before the grasping can lead to a grasping plan that is not suitable to the situation. Our goal is to prevent the consequences of those failures by detecting them promptly and aborting the process when necessary.

\subsection{Overview of the solution}

The problem can be divided in two subproblems: 
\begin{itemize}
\setlength{\itemsep}{0pt}
\setlength{\parskip}{0pt}
\item[--] moving safely the arm towards the goal without colliding with the object ;
\item[--] closing the gripper while the sensors don't show any abnormal behaviour.
\end{itemize}

An abstraction of the decision procedure is shown in Figure \ref{fig:wholeprocess}. We can see the absence of coupling in the two subproblems.

\input{src/decision-procedure.flow}

%In this figure, we can see that the grasp planner uses the robot's incomplete perception in order to decide how the object should be taken.

In this figure, the green rectangle corresponds to the first item in the list above, the approach procedure, which will be discussed in Section \ref{sec:approach}. The blue rectangle, the grasping procedure, will be thoroughly developed in Section \ref{sec:grasping} with much emphasis as it is the main focus of our work.

We need to address the two problems stated by each of these areas, and will do it separately. During the approach phase, if the position chosen by the grasp planner does not lead to a valid motion plan, we will need to pick the object from elsewhere. When the robot is moving, the sensors should be monitored to detect collisions with obstacles and stop immediately. Once the goal given by the part-based planner is reached, the actual grasping happens. There, while the gripper closes, the tactile sensors on the phalanges are monitored and the gripper's state as well as their values are sent to an algorithm that will decide whether the grasping is failing or not. If it is, the gripper is stopped and the process has to be rerun.

We will present this work in a sequential order by going over the related work, then briefly describing the hardware that was available, explain how each phase is handled with a great emphasis on the grasping itself and finally present the results.