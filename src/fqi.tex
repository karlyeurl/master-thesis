\subsection{FQI}
The fitted Q iteration (FQI) \cite{FQI} is an algorithm that aims to determine an optimal control policy for an agent through reinforcement learning. This control policy uses the expected reward on the long run for every action that can be taken given a state and then chooses the action maximizing that expected reward. The agent is thus able to immediately take decisions with the long-term objective in mind instead of choosing the action that will give the highest instantaneous reward.

FQI builds a model that tries to iteratively approximate this optimal policy by learning over $\mcal{F}$, which is a set of 4-tuples $(\mbf{s}_t, a_t, r_t, \mbf{s}_{t+1})$ where $\mbf{s}_{t+1}$ is the state attained from $\mbf{s}_t$ when choosing to take the action $a_t$, and $r_t$ being the immediate reward for that transition.

Every action $a$ that can be taken at any moment belongs to the set $\mcal{A}$. In our case, $\mcal{A} = \{0, 1\}$ where $0$ is the action of stopping and $1$ allows to continue.

Every iteration $i$ builds a function $\mcal{Q}^i(\mbf{s}_t, a_t)$ that approximates the function $\mcal{Q}(\mbf{s}_t, a_t)$ of the expected long-term reward for a given state and action by the means of a supervised learning algorithm $\mcal{R}$. The discount factor, $\gamma \in [0, 1]$, weighs the importance of the long-term over the immediate reward and ensures that the estimator $\mcal{Q}^i$ converges towards $\mcal{Q}$. Algorithm \ref{alg:fqi-algo} details the algorithm behind FQI: at each iteration, we build a model that learns over a set of inputs, representing a state and an action, whose outputs is their expected reward computed by a weighted sum of their immediate reward and the expected reward of the next state predicted by the previous model.

\begin{algorithm}[H]
\caption{fitted Q iteration}\label{alg:fqi-algo}
\begin{algorithmic}[1]
\Procedure{fitted Q iteration}{$\mcal{F}$, $\mcal{R}$}
 \State $N \gets 0$
 \State $\mcal{Q}^0$ is 0 for every input \Comment{Estimator such that all input gives 0}
 \While{$N < total\_iterations$}
 \State{Build $\mcal{TS}^N = f(\mcal{Q}^N, \mcal{F}) = \{(\mbf{i}^l, o^l), l = 1, \dots, \#\mcal{F}\}$} where
\begin{equation*}\begin{split}
 \mbf{i}^l &= (\mbf{s}_t^l, a_t^l)\\
 o^l &= r_t^l + \gamma \max_{a \in \mcal{A}}{\mcal{Q}_N(\mbf{s}^l_{t+1}, a)}
\end{split}\end{equation*}
 \State{$\mcal{Q}^{N+1} \gets \mcal{R}(\mcal{TS}^N)$}
 \State{$N \gets N+1$}
 \EndWhile
 \State \Return{$\mcal{Q}^N$}
\EndProcedure
\end{algorithmic}
\end{algorithm}

\subsubsection{Modelling the system's state for FQI}\label{subs:fqi-model}

In our model for this system, we need to keep several details in mind. Every individual state must hold some history from the previous states, especially for lost contacts, and the state's dimension has to be reasonable.

It was decided to model a state $\tbf{s} \in \mcal{S}_{FQI}$ according to the following definition:

$$\tbf{s} = (\mbf{t}, \mbf{f}, k)$$

where $\mbf{t} = (h_{1}, h_{2}, h_{4}, h_{5}, h_{7}, h_{8}, h_{10} )$ with $h_a \in \mbb{Z}$ for $a \in \{1, 2, 4, 5, 7, 8, 10\}$ corresponding to the tactile information for each cell, $\mbf{f} = (f_{1}, f_{2}, f_{3})$ corresponds to the triplet of the fingers' positions with $f_{1}, f_{2}$ and $f_{3} \in \mbb{N}$ and $\leq 255$ and $k \in \{0, 1\}$ is always set to 0 unless $\tbf{s}$ is a terminal state.

The tactile information is processed from the values of $\tbf{c}_i$ described in Section \ref{subs:datadef} as follows: if more than $x_n$ cells from sensor $n$ are above a threshold $z$ (whose value will depend on the sensitivity), the sensor is labeled as active. Active sensors are given a positive value corresponding to the amount of steps since they have been active. If an active sensor does not record a contact anymore, it will be labeled as inactive and will be given a negative value corresponding to the amount of steps since it has been inactive. Sensors that have not been labeled active nor inactive will be given the value 0. An alternative version that will be considered discards the information from inactive sensors and simply labels them to 0.

\tbf{Example of a state:} $\{5, 6, 0, -5, 6, -1, 0, 88, 159, 160, 0\}$

\subsubsection{Modelling the actions and the reward system for FQI}

It is important to choose wisely the reward given for each action over time. As we want to stop as early as possible if the grasping fails, we must assign a negative reward each time we choose to continue so that it \tit{costs} something to continue. Then, at the final state, either the grasping is a success, in which case we assign a significant reward that will make the price of continuing affordable, or the grasping is a failure and we don't give any reward.

The motivation behind this is to achieve to build a model that will continue as long as it is likely to succeed.

As for stopping, we will not give any reward and model the action as a complete shutdown of the process.

\subsubsection{Using the data to build the learning set $\mcal{F}$ for FQI}

For any successive states $\tbf{s}_t$ and $\tbf{s}_{t+1} \in \mcal{S}_{FQI}$ derived from an experiment $\mcal{E}$ where $\tbf{s}_{t+1}$ is not a final state, we add the following 4-tuples $(\mbf{s}_t, a_t, r_t, \mbf{s}_{t+1})$ to the learning set $\mcal{F}$:

\begin{itemize}
      \item[--] $(\tbf{s}_t, 1, -1, \tbf{s}_{t+1})$ as the “continue” action ;
      \item[--] $(\tbf{s}_t, 0, 0, \tbf{s}_{t}')$ as the “stop” action where $\tbf{s}_{t}'$ is similar to $\tbf{s}_{t}$ but with $k = 1$ ;
      \item[--] $(\tbf{s}_t', 0, 0, \tbf{s}_{t}')$ as the “stopped” transition, which is an infinite loop over a state.
\end{itemize}

When $\tbf{s}_{t+1}$ is a final state, we add:

\begin{itemize}
      \item[--] $(\tbf{s}_t, 1, 50, \tbf{s}_{t+1})$ as the “continue” action if $\mcal{E}$ is successful ;
      \item[--] $(\tbf{s}_t, 1, -1, \tbf{s}_{t+1})$ as the “continue” action if $\mcal{E}$ is unsuccessful ;
      \item[--] $(\tbf{s}_t, 0, 0, \tbf{s}_{t}')$ as the “stop” action ;
      \item[--] $(\tbf{s}_t', 0, 0, \tbf{s}_{t}')$ as the “stopped” transition ;
      \item[--] $(\tbf{s}_{t+1}, 1, 0, \tbf{s}_{t+1})$ as the “finished” transition.
\end{itemize}

The stop action is modelled \tit{s.t.} when it is chosen, it enters the infinite loop $\tbf{s}_t → \tbf{s}_t' → \tbf{s}_t' \dots$ Figure \ref{fig:FQI-F-set} shows a visual representation of this: the edges are the transitions represented in $\mcal{F}$ and the nodes are elements of $\mcal{S}_{FQI}$ derived from the set of experiments $\mcal{E}$. The final transition, shown on a dashed arrow, depends on the experiment's label.

\begin{figure}[H]
\begin{adjustwidth}{-1cm}{-0.5cm}
\centering
\begin{tikzpicture}[->,node distance=3cm,>=stealth',auto]

  \tikzstyle{round}=[circle,thick,draw=blue!75,fill=blue!20,minimum size=14mm]
  \tikzstyle{roundblack}=[circle,thick,draw=black!75,fill=black!20,minimum size=14mm]
  \tikzstyle{rectangle}=[rectangle,thick,draw=blue!75,fill=blue!20,minimum size=14mm]
  \tikzstyle{roundred}=[circle,thick,draw=red!75,fill=red!20,minimum size=14mm]
  \tikzstyle{roundgreen}=[circle,thick,draw=green!75,fill=green!20,minimum size=14mm]
  \tikzstyle{roundorange}=[circle,thick,draw=orange!75,fill=orange!20,minimum size=14mm]

  \tikzstyle{every label}=[red]

  \begin{scope}
   \node [roundorange] (s1) {$\tbf{s}_1$};
   \node [roundorange] [right of = s1] (s2) {$\tbf{s}_2$};
   \node [roundorange] [right of = s2, node distance=5cm] (s24) {$\tbf{s}_{24}$};
   \node [roundgreen] [right of = s24, yshift=2cm] (s25s) {$\tbf{s}_{25}$};
   \node [roundred] [right of = s24, yshift=-2cm] (s25f) {$\tbf{s}_{25}$};
   \node [roundblack] [below of = s1](q1) {$\tbf{s}_1$};
   \node [roundblack] [below of = s2] (q2) {$\tbf{s}_2$};
   \node [roundblack] [below of = s24](q24) {$\tbf{s}_{24}$};
   % \node [roundorange] [right of = s2, node distance=5cm] (s24) {$\tbf{s}_{24}$};
   % \node [roundgreen] [right of = s24, yshift=2cm] (s25s) {$\tbf{s}_{25}$};
   % \node [roundred] [right of = s24, yshift=-2cm] (s25f) {$\tbf{s}_{25}$};

    \path[every node/.style={font=\sffamily\small}]
    (s1) edge node[text width=1.5cm, align=center] {continue\\$r=-1$} (s2)
    (s1) edge node[text width=1.5cm, align=center] {stop\\$r=0$} (q1)
    (s2) edge node[text width=1.5cm, align=center] {stop\\$r=0$} (q2)
    (s24) edge node[text width=1.5cm, left, align=center] {stop\\$r=0$} (q24)
    (q1) edge [loop below, min distance=60pt, in=-60, out=-120] node[below, align=center, text width=1.5cm] {stop\\$r=0$} (q1)
    (s2) edge node[text width=1.5cm, align=center] {$\dots$\\~} (s24)
    (q2) edge [loop below, min distance=60pt, in=-60, out=-120] node[below, align=center, text width=1.5cm] {stop\\$r=0$} (q2)
    (s24) edge[-,dashed] ++(1.5,0) node[]{}++(1.5,0) edge[dashed] node[align=center, above, sloped] {$r=50$} (s25s)
    (s24) edge[-,dashed] ++(1.5,0) node[]{}++(1.5,0) edge[dashed] node[align=center, below, sloped] {$r=-1$} (s25f)
    (q24) edge [loop below, min distance=60pt, in=-60, out=-120] node[below, align=center, text width=1.5cm] {stop\\$r=0$} (q24)
    (s25s) edge [loop right, min distance=60pt, in=30, out=-30] node[right] {$r=0$} (s25s)
    (s25f) edge [loop right, min distance=60pt, in=30, out=-30] node[right] {$r=0$} (s25f);
    \end{scope}
\end{tikzpicture}
\caption{Representation of the transitions added to the set $\mcal{F}$ for an experiment}
\label{fig:FQI-F-set}
\end{adjustwidth}
\end{figure}

\subsubsection{Usage of the generated $\mcal{Q}$ function}\label{subsubs:useQ}
In this section, we will try to give a good idea on how the model generated works and how it is used. After running FQI, we end up with a function $\mcal{Q}^N$ which returns the expected reward given a state and an action. All there is left to do is then to find the action that leads to the best expected outcome.

We define $\displaystyle f(\tbf{s}) = \argmax_{a \in \mcal{A}}{\mcal{Q}^N(\mbf{s}, a)}$, the decision function which chooses the action that gives the maximal expected long-term reward. This is the function that will finally be used to decide whether we stop or continue. Figure \ref{fig:max-reward} shows how $f(\tbf{s})$ allows to take a decision as soon as the current state is known and the outcome of $f(\tbf{s})$ is computed.

\begin{figure}[H]
\centering
\begin{tikzpicture}[->,node distance=3cm,>=stealth',auto]

  \tikzstyle{round}=[circle,thick,draw=blue!75,fill=blue!20,minimum size=15mm]
  \tikzstyle{roundblack}=[circle,thick,draw=black!75,fill=black!20,minimum size=15mm]
  \tikzstyle{rectangle}=[rectangle,thick,draw=blue!75,fill=blue!20,minimum size=15mm]
  \tikzstyle{roundred}=[circle,thick,draw=red!75,fill=red!20,minimum size=15mm]
  \tikzstyle{roundgreen}=[circle,thick,draw=green!75,fill=green!20,minimum size=15mm]
  \tikzstyle{roundorange}=[circle,thick,draw=orange!75,fill=orange!20,minimum size=15mm]

  \tikzstyle{every label}=[red]

  \begin{scope}
   \node [roundorange] (s1) {$f(\tbf{s}_1)$};
   \node [roundorange] [right of = s1] (s2) {$f(\tbf{s}_2)$};
   \node [roundorange] [right of = s2, node distance=5cm] (s24) {$f(\tbf{s}_{24})$};
   \node [roundgreen] [right of = s24] (s25s) {OK};
   \node [roundred] [below of = s24, xshift=-2.5cm, yshift=-1cm] (s25f) {Stop};

    \path[every node/.style={font=\sffamily\small}]
    (s1) edge node[text width=1.5cm, align=center] {$1$} (s2)
    (s2) edge node[text width=1.5cm, align=center] {$1, \dots$} (s24)
    (s24) edge node[align=center] {$1$} (s25s)
    (s1) edge[bend right] node[align=center, above, sloped] {$0$} (s25f)
    (s2) edge[bend right] node[align=center, above, sloped] {$0$} (s25f)
    (s24) edge[bend left] node[align=center, above, sloped] {$0$} (s25f);
    \end{scope}
\end{tikzpicture}
\caption{Usage of the decision function through an experiment}
\label{fig:max-reward}
\end{figure}

\subsubsection{Artificially adding experimental data}

This subsection will present a little trick allowing us to expand the experimental data as much as possible.

If we assume that the sensors have a sufficiently similar behaviour regardless of the finger they are attached to and that the object we are trying to grasp is symmetrical, we can consider that, by permuting the data from fingers 1 and 3, we find the data for the mirrored case.

This effectively doubles the amount of experimental data available to build $\mcal{Q}$, which can only lead to a better model. Figure \ref{fig:mirror-trick} shows an example of a configuration where we could use only one measurement to represent both cases. In practice, most objects are symmetrical and this idea can be applied very often.

\begin{figure}[H]
\begin{adjustwidth}{-1cm}{-1cm}
  \begin{center}
    \begin{subfigure}{0.5\textwidth}
      \centering
      \includegraphics[trim = 80px 160px 340px 150px, clip, width=\textwidth]{img/mirror1}
    \end{subfigure}%
    ~
    \begin{subfigure}{0.5\textwidth}
      \centering
      \includegraphics[trim = 70px 160px 350px 150px, clip, width=\textwidth]{img/mirror2}
    \end{subfigure}
  \caption{Example of a mirrored case}\label{fig:mirror-trick}
  \end{center}
 \end{adjustwidth}
\end{figure}