\select@language {english}
\contentsline {section}{\numberline {1}Introduction}{1}{section.2}
\contentsline {subsection}{\numberline {1.1}Grasping objects: a multifaceted problem}{1}{subsection.3}
\contentsline {subsection}{\numberline {1.2}Description of the subproblem of interest}{1}{subsection.4}
\contentsline {subsection}{\numberline {1.3}Sources of failure}{1}{subsection.5}
\contentsline {subsection}{\numberline {1.4}Overview of the solution}{2}{subsection.6}
\contentsline {section}{\numberline {2}Related Work}{4}{section.8}
\contentsline {section}{\numberline {3}Hardware description}{6}{section.9}
\contentsline {subsection}{\numberline {3.1}Actuated arm}{6}{subsection.10}
\contentsline {subsection}{\numberline {3.2}Gripper}{7}{subsection.12}
\contentsline {subsubsection}{\numberline {3.2.1}Underactuation: a blessing and a curse}{7}{subsubsection.14}
\contentsline {subsection}{\numberline {3.3}Sensing devices}{8}{subsection.15}
\contentsline {subsubsection}{\numberline {3.3.1}Vision}{8}{subsubsection.16}
\contentsline {subsubsection}{\numberline {3.3.2}Load cells}{8}{subsubsection.18}
\contentsline {subsection}{\numberline {3.4}Mounting}{9}{subsection.21}
\contentsline {section}{\numberline {4}Trajectory generation and execution}{11}{section.23}
\contentsline {subsection}{\numberline {4.1}Preliminary concerns}{11}{subsection.24}
\contentsline {subsubsection}{\numberline {4.1.1}On trajectory generation}{12}{subsubsection.27}
\contentsline {subsection}{\numberline {4.2}Execution of the motion plan}{12}{subsection.28}
\contentsline {section}{\numberline {5}Grasping}{13}{section.29}
\contentsline {subsection}{\numberline {5.1}Definition of a successful grasp}{13}{subsection.30}
\contentsline {subsection}{\numberline {5.2}Monitoring the sensors}{14}{subsection.33}
\contentsline {subsection}{\numberline {5.3}Formalization of the data that can be acquired}{15}{subsection.34}
\contentsline {subsection}{\numberline {5.4}Learning grasps}{15}{subsection.35}
\contentsline {subsection}{\numberline {5.5}FQI}{19}{subsection.40}
\contentsline {subsubsection}{\numberline {5.5.1}Modelling the system's state for FQI}{20}{subsubsection.42}
\contentsline {subsubsection}{\numberline {5.5.2}Modelling the actions and the reward system for FQI}{21}{subsubsection.43}
\contentsline {subsubsection}{\numberline {5.5.3}Using the data to build the learning set $\mathcal {F}$ for FQI}{21}{subsubsection.44}
\contentsline {subsubsection}{\numberline {5.5.4}Usage of the generated $\mathcal {Q}$ function}{22}{subsubsection.46}
\contentsline {subsubsection}{\numberline {5.5.5}Artificially adding experimental data}{23}{subsubsection.48}
\contentsline {section}{\numberline {6}Results}{24}{section.50}
\contentsline {subsection}{\numberline {6.1}Choices of parameters and algorithms}{24}{subsection.51}
\contentsline {subsection}{\numberline {6.2}Model for the cylinder prototype}{25}{subsection.52}
\contentsline {subsubsection}{\numberline {6.2.1}Experiments}{25}{subsubsection.54}
\contentsline {subsubsection}{\numberline {6.2.2}Performances of the two models}{27}{subsubsection.57}
\contentsline {subsection}{\numberline {6.3}Attempt at using the box prototype}{29}{subsection.63}
\contentsline {subsection}{\numberline {6.4}Conclusions and Future work}{31}{subsection.67}
\contentsline {section}{\numberline {A}Software interface}{32}{section.68}
\contentsline {subsection}{\numberline {A. I}ROS}{32}{subsection.69}
\contentsline {subsection}{\numberline {A. II}MoveIt! for the UR5}{32}{subsection.70}
\contentsline {subsubsection}{\numberline {A. II.1}Quick word about the OMPL}{33}{subsubsection.71}
\contentsline {subsection}{\numberline {A. III}Robotiq}{33}{subsection.72}
\contentsline {subsection}{\numberline {A. IV}Sensors}{33}{subsection.73}
\contentsline {subsection}{\numberline {A. V}API}{34}{subsection.74}
\contentsline {subsection}{\numberline {A. VI}Implementation details for FQI}{35}{subsection.76}
\contentsline {subsection}{\numberline {A. VII}Implementation details for the decision procedure}{35}{subsection.77}
\contentsline {section}{\numberline {B}Quaternions}{36}{section.78}
\contentsline {subsection}{\numberline {B. I}Mathematical description of quaternions}{36}{subsection.79}
\contentsline {subsubsection}{\numberline {B. I.1}Complex numbers and 2D transforms}{36}{subsubsection.80}
\contentsline {subsubsection}{\numberline {B. I.2}Quaternions for 3D transforms}{36}{subsubsection.82}
\contentsline {subsection}{\numberline {B. II}Implications in 3D geometry (and robotics)}{37}{subsection.83}
\contentsline {subsubsection}{\numberline {B. II.1}Representation of poses in space and transformations}{37}{subsubsection.84}
\contentsline {section}{\numberline {C}Planners analysis}{38}{section.85}
\contentsline {subsection}{\numberline {C. I}Computation time and success rate}{39}{subsection.87}
\contentsline {subsection}{\numberline {C. II}Trajectories analysis}{39}{subsection.89}
\contentsline {subsection}{\numberline {C. III}Choosing the planner}{42}{subsection.95}
\contentsline {section}{Bibliography}{44}{section*.97}
